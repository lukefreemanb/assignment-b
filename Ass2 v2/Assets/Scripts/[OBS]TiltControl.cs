﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiltControl : MonoBehaviour {

    public Transform cameraFollow;

    public float smooth = 0.2F;
    public float tiltAngle = 30.0f;
    private Vector3 tiltAroundZ, tiltAroundX;
    private Quaternion target = Quaternion.Euler(0, 0, 0);

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

        tiltAroundZ = Input.GetAxis("Horizontal") * cameraFollow.right;
        tiltAroundX = Input.GetAxis("Vertical")* cameraFollow.forward;
        //Quaternion target = Quaternion.Euler(tiltAroundX, 0, -tiltAroundZ);

    }

    private void FixedUpdate()
    {
        transform.Rotate((tiltAroundX - tiltAroundZ) * tiltAngle);

        transform.rotation = Quaternion.Lerp(transform.rotation, target, smooth);
    }
}
