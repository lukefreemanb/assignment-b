﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerResetControl : MonoBehaviour {

    private Vector3 startingPosition;

    public float resetPoint = -20f;

	// Use this for initialization
	void Start () {
        startingPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y <= resetPoint)
        {
            transform.position = startingPosition;
        }
	}
}
