﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class L4Spinner : MonoBehaviour
{

    public float rotateSpeed = 10.0f;
    //public Quaternion localRotation;
    //private Vector3 z;

    // Use this for initialization
    void Start()
    {
        transform.rotation = Quaternion.identity;
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        transform.Rotate(Vector3.up * Time.deltaTime * rotateSpeed);
    }
}
