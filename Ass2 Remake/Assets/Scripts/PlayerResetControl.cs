﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerResetControl : MonoBehaviour {

    private Vector3 startingPosition;
    private Rigidbody rb;
    public float resetPoint = -20f;

	// Use this for initialization
	void Start () {
        startingPosition = transform.position;

        rb = this.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y <= resetPoint)
        {
            transform.position = startingPosition;
            rb.velocity = Vector3.zero;
        }
	}
}
