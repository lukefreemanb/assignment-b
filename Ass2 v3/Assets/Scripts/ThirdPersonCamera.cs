﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour {

    public Transform target;
    public float rotateSpeed = 5;
    Vector3 offset;

    Transform thisTransform;

	// Use this for initialization
	void Start () {
        //offset = target.transform.position - transform.position;
        Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update () {
        float x = Input.GetAxis("Mouse X") * rotateSpeed;
        float y = Input.GetAxis("Mouse Y") * rotateSpeed;
        target.Rotate(Vector3.up, -x);
        target.Rotate(Vector3.right, y);
        //target.Rotate(0, x, 0);
        //float desiredAngleY = target.transform.eulerAngles.y;
        // float desiredAngleX = target.transform.eulerAngles.x;
        // Quaternion rotation = Quaternion.Euler(0, x, 0);

        // thisTransform.Rotate(Vector3.right, Input.GetAxis("Mouse Y"));
        // thisTransform.Rotate(Vector3.up, Input.GetAxis("Mouse X"));

        //transform.position = target.transform.position - (rotation * offset);

        transform.LookAt(target.transform);
    }
}
