﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTargetLink : MonoBehaviour {

    public Transform Target;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Target.transform.position;
	}
}
